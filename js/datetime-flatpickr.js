(function (Drupal, drupalSettings) {
  "use strict";

  Drupal.behaviors.DateTimeFlatPickr = {
    attach: function (context) {

      for (var name in drupalSettings.datetimeFlatPickr) {
        const flatpickr = once("datetime-flatpickr", "input[flatpickr-name='" + name + "']");
        flatpickr.forEach(function (item) {
          const { disable, minDate, maxDate, disabledWeekDays, ...settings } = drupalSettings.datetimeFlatPickr[name].settings;
          let extraSettings = {};
          // Min/Max date as number will set the offset with days,
          // otherwise, use date.
          if (minDate) {
            extraSettings.minDate = !isNaN(minDate) ? new Date().fp_incr(minDate) : minDate;
          }
          if (maxDate) {
            extraSettings.maxDate = !isNaN(maxDate) ? new Date().fp_incr(maxDate) : maxDate;
          }

          // Disable dates if present.
          extraSettings.disable = disable ?? [];

          // Disable selected weekdays.
          extraSettings.disable.push(function(date) {
            return disabledWeekDays.includes(date.getDay());
          });

          item.flatpickr({ ...settings, ...extraSettings });
        });
      }
    }
  };

})(Drupal, drupalSettings);
